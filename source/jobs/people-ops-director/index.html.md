---
layout: markdown_page
title: "People Operations Director"
---

## Responsibilities

- **Effectiveness**
    - Keep our guidelines and materials [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) (refer to the updated handbook when emailing instead of duplicating content)
    - Our [handbook](https://about.gitlab.com/handbook/) is the single source of truth
    - Follow the [GitLab workflow](http://doc.gitlab.com/ee/workflow/gitlab_flow.html)
    - Implement best practices such as [A Resource Guide for Recruiting](https://medium.com/for-entrepreneurs/a-resource-guide-for-recruiting-advice-from-the-experts-a41d549880f4#.g9pkqdeq7)
 - **Remote only aspects**
    - Have people feel close through initiatives
    - Structure process efficiently and without duplication
    - Promoting us as a remote only company
- **Training program management**
    - Manage GitLab University
    - Manage Traintool for soft skill training
    - Ensure the rest of the community benefits
- **Recruiting program management**
    - Recruiter will report to you
    - Make sure people are responded to in the ATS
    - Make sure interviewers are trained
    - Make sure candidate evaluation is structured and recorded
    - Manage the onboarding process
    - Manage the offboarding process
    - Referral program management
    - Assist managers with job descriptions
- **Retention program management**
    - eNPS/ happiness measurement
    - Personal development programs
    - Help the CEO answer team member Feedback Form questions
    - Make things easier from the perspective of the team members
- **Performance Management**
    - Implement global program for evaluating performance and providing feedback
    - Work to tie performance management to compensation structure to professional development (i.e. promotion)
- **Compensation structure**
    - Compensation guidelines that work worldwide but tailored to local markets
    - Implement compensation process that allows input from managers but removes direct authority for compensation changes
    - Review compensation and do pre-emptive raises
- **HR Compliance**
    - Manage information flow for US payroll and benefits provider TriNet
    - Evaluate need and timing for conversion from PEO (Trinet) to internally managed HR function.
    - Manage the process of changes to team members compensation, position, etc.
    - Deal with legal issues related to employment
    - Take lead in delicate People Operations issues (special circumstances, conflicts, sickness, layoffs, etc.)


## Requirements

- Experience with US employment law and best practices
- Experience with human resource in countries other than the US.
- Excellent written and verbal communication skills
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Wanting to work for a fast-moving startup
